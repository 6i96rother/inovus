package org.inovus.data;

import org.inovus.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Implementation of {@link JpaRepository}.
 * Aims to perform {@link Authority}'s CRUD operations.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
public interface AuthorityRepository extends JpaRepository<Authority, Integer> {
}
