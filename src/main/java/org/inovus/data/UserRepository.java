package org.inovus.data;

import org.inovus.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Implementation of {@link JpaRepository}.
 * Aims to perform {@link User}'s CRUD operations.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
public interface UserRepository extends JpaRepository<User, Integer> {

    User getByUsernameIgnoreCase(String username);

}
