package org.inovus.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.persistence.EntityManagerFactory;

/**
 * Spring's root configuration class.
 * Contains main bean definitions.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
@Configuration
@ComponentScan(value = "org.inovus",
        excludeFilters = {
                @Filter(type= FilterType.ANNOTATION, value = EnableWebMvc.class)
        })
@EnableTransactionManagement
public class RootConfig {

        private ProjectConstants constants;

        @Autowired
        public void setConstants(ProjectConstants constants) {
                this.constants = constants;
        }

        @Bean
        public BasicDataSource dataSource() {

                // create data source
                BasicDataSource ds = new BasicDataSource();
                ds.setDriverClassName(constants.DB_DRIVER_NAME);
                ds.setUrl(constants.DB_CONNECTION_URL);
                ds.setUsername(constants.DB_AUTH_USER);
                ds.setPassword(constants.DB_AUTH_PSWD);
                ds.setInitialSize(constants.DB_INITIAL_SIZE);

                return ds;

        }

        @Bean
        DatabasePopulator databaseInit() {

                ResourceDatabasePopulator populator
                        = new ResourceDatabasePopulator();

                populator.setScripts(
                        new ClassPathResource(constants.DB_INITIAL_SCRIPT));

                return populator;

        }

        @Bean
        DatabasePopulator databaseClean() {

                ResourceDatabasePopulator populator
                        = new ResourceDatabasePopulator();

                populator.setScripts(
                        new ClassPathResource(constants.DB_CLEANUP_SCRIPT));

                return populator;

        }

        @Bean
        DataSourceInitializer dataSourceInitializer() {

                DataSourceInitializer initializer
                        = new DataSourceInitializer();

                initializer.setDataSource(dataSource());
                initializer.setDatabasePopulator(databaseInit());
                initializer.setDatabaseCleaner(databaseClean());

                return initializer;

        }

        @Bean
        JpaVendorAdapter jpaVendorAdapter() {

                HibernateJpaVendorAdapter adapter
                        = new HibernateJpaVendorAdapter();
                adapter.setDatabase(Database.HSQL);
                adapter.setShowSql(true);
                adapter.setGenerateDdl(false);
                adapter.setDatabasePlatform("org.hibernate.dialect.HSQLDialect");

                return adapter;

        }

        @Bean
        public LocalContainerEntityManagerFactoryBean entityManagerFactory() {

                LocalContainerEntityManagerFactoryBean entityManager =
                        new LocalContainerEntityManagerFactoryBean();
                entityManager.setDataSource(dataSource());
                entityManager.setPackagesToScan("org.inovus.entity");
                entityManager.setJpaVendorAdapter(jpaVendorAdapter());

                return entityManager;

        }

        @Bean
        public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
                return new JpaTransactionManager(entityManagerFactory);
        }

        @Bean
        public BeanPostProcessor persistenceTranslation() {
                return new PersistenceExceptionTranslationPostProcessor();
        }

        @Bean
        public MessageSource messageSource() {

                ReloadableResourceBundleMessageSource messageSource
                        = new ReloadableResourceBundleMessageSource();
                messageSource.setDefaultEncoding("UTF-8");
                messageSource.addBasenames("locale/messages");

                return messageSource;

        }

}