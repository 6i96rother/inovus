package org.inovus.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Spring's security web initializer class.
 * Aims to intercept and delegate incoming requests.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
public class SecutiryWebInitializer extends AbstractSecurityWebApplicationInitializer {
}
