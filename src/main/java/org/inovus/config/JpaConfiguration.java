package org.inovus.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Spring's JPA configuration class.
 * Aims to locate JPA repositories.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
@Configuration
@EnableJpaRepositories(basePackages = "org.inovus.data")
public class JpaConfiguration {
}
