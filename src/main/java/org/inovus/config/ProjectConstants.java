package org.inovus.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Class for containing project constants,
 * located in property files.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
@Component
@PropertySource("config/config.properties")
public class ProjectConstants {

    // database
    public final String DB_DRIVER_NAME;
    public final String DB_CONNECTION_URL;
    public final String DB_AUTH_USER;
    public final String DB_AUTH_PSWD;
    public final String DB_CHARSET;
    public final String DB_INITIAL_SCRIPT;
    public final String DB_CLEANUP_SCRIPT;
    public final int DB_INITIAL_SIZE;

    // security
    public final String SECURITY_SECRET_KEY;
    public final String SECURITY_ROLE_USER;
    public final String SECURITY_ROLE_ADMIN;
    public final int SECURITY_USERNAME_MINLENGTH;
    public final int SECURITY_USERNAME_MAXLENGTH;
    public final int SECURITY_PASSWORD_MINLENGTH;
    public final int SECURITY_PASSWORD_MAXLENGTH;

    @Autowired
    public ProjectConstants(Environment env) {

        DB_DRIVER_NAME = env.getRequiredProperty("db.driver.name");
        DB_CONNECTION_URL = env.getRequiredProperty("db.connection.url");
        DB_AUTH_USER = env.getRequiredProperty("db.auth.user");
        DB_AUTH_PSWD = env.getRequiredProperty("db.auth.pswd");
        DB_CHARSET = env.getRequiredProperty("db.charset");
        DB_INITIAL_SCRIPT = env.getRequiredProperty("db.initial.script");
        DB_CLEANUP_SCRIPT = env.getRequiredProperty("db.cleanup.script");
        DB_INITIAL_SIZE = env.getRequiredProperty("db.initial.size", Integer.class);

        SECURITY_SECRET_KEY = env.getRequiredProperty("security.secret.key");
        SECURITY_ROLE_USER = env.getRequiredProperty("security.role.user");
        SECURITY_ROLE_ADMIN = env.getRequiredProperty("security.role.admin");
        SECURITY_USERNAME_MINLENGTH = env.getRequiredProperty(
                "security.username.minlength", Integer.class);
        SECURITY_USERNAME_MAXLENGTH = env.getRequiredProperty(
                "security.username.maxlength", Integer.class);
        SECURITY_PASSWORD_MINLENGTH = env.getRequiredProperty(
                "security.password.minlength", Integer.class);
        SECURITY_PASSWORD_MAXLENGTH = env.getRequiredProperty(
                "security.password.maxlength", Integer.class);

    }

}
