package org.inovus.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity class that represents system users.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
@Entity
@Table(name="users")
public class User {

    private String username;
    private String password;
    private String confirmation;
    private boolean enabled;

    Set<Authority> authorities = new HashSet<>();

    public User() {
    }

    public User(String username, String password, boolean enabled) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }

    @Id
    @Column(name="username")
    public String getUsername() {
        return username;
    }

    @Column(name="password")
    public String getPassword() {
        return password;
    }

    @Column(name="enabled")
    public boolean isEnabled() {
        return enabled;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    public Set<Authority> getAuthorities() {
        return authorities;
    }

    @Transient
    public String getConfirmation() {
        return confirmation;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }

    public void addAuthority(String authority) {
        authorities.add(new Authority(this, authority));
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

}