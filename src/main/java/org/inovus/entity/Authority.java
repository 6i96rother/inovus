package org.inovus.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity class that represents {@link User}'s roles.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
@Entity
@Table(name="authorities")
public class Authority implements Serializable {

    private String id;
    private User user;
    private String authority;

    public Authority() {
    }

    public Authority(User user, String authority) {
        this.user = user;
        this.authority = authority;
    }

    @Id
    @GeneratedValue
    public String getId() {
        return id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "username")
    public User getUser() {
        return user;
    }

    @Column(name="authority")
    public String getAuthority() {
        return authority;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        return "Authority{" +
                "authority='" + authority + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Authority authority1 = (Authority) o;

        return authority.equals(authority1.authority);
    }

}