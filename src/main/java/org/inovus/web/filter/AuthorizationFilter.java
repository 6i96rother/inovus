package org.inovus.web.filter;

import org.inovus.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter for "sign-in", "sign-up" pages.
 *
 * @author Igor Fadeev
 * @version 1.0
 */

@Component
public class AuthorizationFilter implements Filter {

    private SecurityService securityService;

    @Autowired
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (securityService.getLoggedInUsername() != null) {
            ((HttpServletResponse) response).sendRedirect("/inovus/welcome");
        }

        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {

    }

}
