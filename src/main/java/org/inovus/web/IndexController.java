package org.inovus.web;

import org.apache.log4j.Logger;
import org.inovus.service.UserServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Controller for index page.
 *
 * @author Igor Fadeev
 * @version 1.0
 */

@Controller
@RequestMapping("/")
public class IndexController {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

    @RequestMapping(method = GET)
    public String doGetIndex() {
        return "redirect: welcome";
    }

    @RequestMapping(value = "welcome", method = GET)
    public String doGet() {
        return "index";
    }

    @ExceptionHandler(Exception.class)
    public String handleException(Exception e, Model model) {

        logger.error(e.getMessage());

        model.addAttribute("errorMessage",
                "Произошла непредвиденная ошибка");

        return "index";

    }

}
