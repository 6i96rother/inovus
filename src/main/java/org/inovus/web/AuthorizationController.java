package org.inovus.web;

import org.apache.log4j.Logger;
import org.inovus.service.UserServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Controller for {@link org.inovus.entity.User}'s authorization.
 *
 * @author Igor Fadeev
 * @version 1.0
 */

@Controller
@RequestMapping("/sign-in")
public class AuthorizationController {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

    @RequestMapping(method = GET)
    public String doGet(Model model, String logout, String error) {

        if (logout != null) {
            model.addAttribute("errorMessage",
                    "Вы вышли из приложения");
        }

        if (error != null) {
            model.addAttribute("errorMessage",
                    "Имя пользователя и пароль не подходят");
        }

        return "sign-in";

    }

    @ExceptionHandler(Exception.class)
    public String handleException(Exception e, Model model) {

        logger.error(e.getMessage());

        model.addAttribute("errorMessage",
                "Произошла непредвиденная ошибка");

        return "sign-in";

    }

}
