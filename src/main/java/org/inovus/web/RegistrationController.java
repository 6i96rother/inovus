package org.inovus.web;

import org.apache.log4j.Logger;
import org.inovus.entity.User;
import org.inovus.service.SecurityService;
import org.inovus.service.UserService;
import org.inovus.service.UserServiceImpl;
import org.inovus.service.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Controller for {@link org.inovus.entity.User}'s registration.
 *
 * @author Igor Fadeev
 * @version 1.0
 */

@Controller
@RequestMapping("/sign-up")
public class RegistrationController {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

    private UserService userService;
    private SecurityService securityService;
    private UserValidator userValidator;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Autowired
    public void setUserValidator(UserValidator userValidator) {
        this.userValidator = userValidator;
    }

    @RequestMapping(method = GET)
    public String doGet(Model model) {

        model.addAttribute("userForm", new User());

        return "sign-up";

    }

    @RequestMapping(method = POST)
    public String doPost(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {

        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "sign-up";
        }

        userService.regiser(userForm.getUsername(),
                userForm.getPassword(), userForm.getConfirmation());

        securityService.autologin(userForm.getUsername(), userForm.getPassword());

        return "redirect: welcome";
    }

    @ExceptionHandler(Exception.class)
    public String handleException(Exception e, Model model) {

        logger.error(e.getMessage());

        model.addAttribute("errorMessage",
                "Произошла непредвиденная ошибка");

        return "sign-up";

    }

}
