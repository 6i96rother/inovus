package org.inovus.service;

/**
 * Service interface for login {@link org.inovus.entity.User}s and getting their details.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
public interface SecurityService {

    String getLoggedInUsername();

    void autologin(String username, String password);

}
