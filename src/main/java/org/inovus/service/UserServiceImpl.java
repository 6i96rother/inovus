package org.inovus.service;

import org.inovus.config.ProjectConstants;
import org.inovus.data.AuthorityRepository;
import org.inovus.data.UserRepository;
import org.inovus.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Implementation of {@link UserService}.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
@Component
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private AuthorityRepository authorityRepository;

    private PasswordEncoder passwordEncoder;
    private ProjectConstants constants;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setAuthorityRepository(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setConstants(ProjectConstants constants) {
        this.constants = constants;
    }

    @Override
    public void regiser(String username, String password, String confirmation) {

        // delete side spaces
        username = username.trim();

        User user = new User(username,
                passwordEncoder.encode(password), true);
        user.addAuthority(constants.SECURITY_ROLE_USER);

        userRepository.save(user);
        authorityRepository.saveAll(user.getAuthorities());

    }

    @Override
    public User authorize(String username, String password) {

        User user = userRepository.getByUsernameIgnoreCase(username);

        return user;

    }

}