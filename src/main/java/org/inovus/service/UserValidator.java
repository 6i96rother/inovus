package org.inovus.service;

import org.inovus.config.ProjectConstants;
import org.inovus.data.UserRepository;
import org.inovus.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of {@link Validator}.
 * Aims to validate user's input values.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
@Component
public class UserValidator implements Validator {

    private UserRepository userRepository;
    private ProjectConstants constants;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setConstants(ProjectConstants constants) {
        this.constants = constants;
    }

    private boolean findSymbols(String line, String expression) {

        Pattern exclusive = Pattern.compile(expression);
        Matcher matcher = exclusive.matcher(line);

        return matcher.find();

    }

    private boolean nameIsValid(String username) {

        if (username.length() < constants.SECURITY_USERNAME_MINLENGTH) {
            return false;
        }

        if (username.length() > constants.SECURITY_USERNAME_MAXLENGTH) {
            return false;
        }

        // name must contain only latin
        // alphabet symbols and digits
        if (findSymbols(username, "[^0-9A-Za-z]")) {
            return false;
        }

        return true;

    }

    private boolean passwordIsValid(String password) {

        if (password.length() < constants.SECURITY_PASSWORD_MINLENGTH) {
            return false;
        }

        if (password.length() > constants.SECURITY_PASSWORD_MAXLENGTH) {
            return false;
        }

        // password must contain at least
        // one letter in upper case
        if (!findSymbols(password, "[A-Z]")) {
            return false;
        }

        // password must contain at least
        // one letter in lower case
        if (!findSymbols(password, "[a-z]")) {
            return false;
        }

        // password must contain at least one digit
        if (!findSymbols(password, "[0-9]")) {
            return false;
        }

        return true;

    }

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(@Nullable Object target, Errors errors) {

        User user = (User) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "validation.empty.fields");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "validation.empty.fields");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmation", "validation.empty.fields");

        if (!user.getPassword().equals(user.getConfirmation())) {
            errors.rejectValue("confirmation", "validation.password.confirmation");
        }

        if (!nameIsValid(user.getUsername())) {
            errors.rejectValue("username", "validation.username.format");
        }

        if (!passwordIsValid(user.getPassword())) {
            errors.rejectValue("password", "validation.password.format");
        }

        if (userRepository.getByUsernameIgnoreCase(user.getUsername()) != null) {
            errors.rejectValue("username", "validation.username.duplicate");
        }

    }

}
