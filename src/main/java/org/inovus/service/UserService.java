package org.inovus.service;

import org.inovus.entity.User;

/**
 * Service interface for register and authorize {@link org.inovus.entity.User}s.
 *
 * @author Igor Fadeev
 * @version 1.0
 */
public interface UserService {

    void regiser(String username, String password, String confirmation);

    User authorize(String username, String password);

}
