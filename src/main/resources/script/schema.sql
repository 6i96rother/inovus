
-- users

CREATE TABLE users (
    username VARCHAR(50),
    password VARCHAR(100),
    enabled BOOLEAN NOT NULL,
    PRIMARY KEY (username)
);

-- authorities

CREATE TABLE authorities (
  id INT IDENTITY,
  username VARCHAR(50) NOT NULL,
  authority VARCHAR(50) NOT NULL,
  FOREIGN KEY (username)
	REFERENCES users (username)
	ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (id)
);

-- groups

CREATE TABLE groups (
  id INT IDENTITY,
  group_name VARCHAR(50) NOT NULL UNIQUE,
  PRIMARY KEY (id)
);

-- group members

CREATE TABLE group_members (
  username VARCHAR(50) NOT NULL,
  group_id INT NOT NULL,
  FOREIGN KEY (username)
    REFERENCES users (username)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (group_id)
    REFERENCES groups (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  PRIMARY KEY (username, group_id)
);

-- group authorities

CREATE TABLE group_authorities (
  group_id INT NOT NULL,
  authority VARCHAR(50) NOT NULL,
  FOREIGN KEY (group_id)
    REFERENCES groups (id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (group_id, authority)
);